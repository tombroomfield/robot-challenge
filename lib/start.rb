# frozen_string_literal: true

require_relative 'broomfield_robot'

robot = Robot.new
robot.register_board(Board.new)

robot.command do
  puts 'AWAITING COMMAND'
  gets.chomp
end
