# frozen_string_literal: true

# Basic reporter object for the robot intended to output reports to the command
# line.
class TerminalReporter
  class << self
    def report(gps_chip)
      if gps_chip.position
        data(gps_chip).join(',').tap { |i| puts i }
      else
        'NOTHING TO REPORT'.tap { |i| puts i }
      end
    end

    private

    def data(gps_chip)
      [gps_chip.position.x, gps_chip.position.y, gps_chip.bearing.to_s.upcase]
    end
  end
end
