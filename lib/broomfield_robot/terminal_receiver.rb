# frozen_string_literal: true

# Basic receiver object for the robot intended to parse inputs from the command
# line.
class TerminalReceiver
  attr_accessor :robot, :input
  ALLOWED_COMMANDS = %w[left right move report].freeze
  EXIT_COMMANDS = %w[exit e quit cancel].freeze

  def command(block)
    return false unless robot

    until EXIT_COMMANDS.include?(input)
      parse_command if input

      # Get new command
      self.input = clean_command(block.call)
    end
  end

  def single_command(incoming_command)
    self.input = clean_command(incoming_command)
    parse_command
  end

  private

  def clean_command(incoming_command)
    return false unless incoming_command
    incoming_command.downcase.strip
  end

  def parse_command
    return parse_place_command if input.include?('place')
    parse_other_command
  end

  def parse_place_command
    place_data = input.gsub('place', '').strip.split(',').map(&:strip)
    return invalid_command unless valid_place_command?(place_data)
    robot.place(*place_data)
  end

  def parse_other_command
    return robot.send(input) if ALLOWED_COMMANDS.include?(input)
    invalid_command
  end

  def invalid_command
    puts 'INVALID COMMAND'
  end

  def valid_place_command?(place_data)
    return false unless place_data.count == 3
    return false unless Compass::RULES.keys.include?(place_data[-1].downcase
                                                                   .to_sym)
    true
  end
end
