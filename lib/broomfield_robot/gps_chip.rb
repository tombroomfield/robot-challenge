# frozen_string_literal: true

# A GPS Chip is a join between a robot and a board.
# It keeps track of the position of the robot on a specifc board object.

# This would allow us to grow in the future to have a robot exist on a variety
# of boards at once - relative position in 'country' would be different
# to 'city'
class GpsChip
  attr_accessor :board, :position, :bearing

  def initialize(board)
    self.board = board
  end

  def place(x, y, bearing)
    return false unless valid_placement?(x, y, bearing)
    self.position = Struct.new(:x, :y).new(x, y)
    self.bearing = bearing
  end

  def move
    return false unless position
    adjust_positions
    correct_falling
  end

  def left
    return false unless position
    self.bearing = compass.left
  end

  def right
    return false unless position
    self.bearing = compass.right
  end

  private

  def adjust_positions
    position.x = position.x + compass.to_movement.x
    position.y = position.y + compass.to_movement.y
  end

  def correct_falling
    correct_x
    correct_y
  end

  def correct_x
    position.x = [[position.x, 0].max, board.dimensions.width].min
  end

  def correct_y
    position.y = [[position.y, 0].max, board.dimensions.height].min
  end

  def compass
    Compass.new(bearing)
  end

  def valid_placement?(x, y, bearing)
    return false unless (0..board.dimensions.width).cover?(x)
    return false unless (0..board.dimensions.height).cover?(y)
    return false unless Compass::RULES.keys.include?(bearing)
    true
  end
end
