# frozen_string_literal: true

# The robot represents the object moving across the board. The Robot
# doesn't actually hold any real logic, but provides a sensible API to control
# it. All commands are then delegated off to the appropriate objects.
class Robot
  attr_accessor :gps_chips, :reporter, :receiver

  def initialize(**opts)
    self.gps_chips = Set.new
    self.reporter = opts.fetch(:reporter) { TerminalReporter }
    self.receiver = opts.fetch(:receiver) { TerminalReceiver.new }
    receiver.robot = self
  end

  def command(&block)
    receiver.command(block)
  end

  def register_board(board)
    gps_chips.add(GpsChip.new(board))
  end

  def place(x, y, bearing)
    gps_chips.each { |c| c.place(x.to_i, y.to_i, bearing.to_sym) }
  end

  def move
    gps_chips.each(&:move)
  end

  def left
    gps_chips.each(&:left)
  end

  def right
    gps_chips.each(&:right)
  end

  def report
    gps_chips.inject('') do |reports, chip|
      reports + reporter.report(chip).to_s
    end
  end
end
