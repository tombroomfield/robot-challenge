# frozen_string_literal: true

# Basic board width/height management as well as validation.
# Effectively a struct with light validation.
class Dimensions
  attr_accessor :width, :height

  def initialize(width, height)
    self.width = width.to_i
    self.height = height.to_i
    invalid unless valid?
  end

  private

  def valid?
    return false if width <= 0
    return false if height <= 0
    true
  end

  def invalid
    raise InvalidDimensionsError, self
  end
end
