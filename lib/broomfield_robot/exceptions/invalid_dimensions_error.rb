# frozen_string_literal: true

# When a dimensions object, usually on a board, is invalid.
class InvalidDimensionsError < StandardError
  def initialize(dimensions)
    super(build_message(dimensions))
  end

  private

  def build_message(dimensions)
    "Tried to create an invalid dimensions: #{dimensions.inspect}, both width "\
    'and height must be positive.'
  end
end
