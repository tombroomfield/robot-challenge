# frozen_string_literal: true

# A board is the simple area in which an object can traverse.
# Boards default to a width and height of 5, but this can be altered on
# instantiation.
class Board
  DEFAULT_WIDTH = 5
  DEFAULT_HEIGHT = 5

  attr_accessor :dimensions

  def initialize(**opts)
    self.dimensions = Dimensions.new(*initial_dimensions(opts))
  end

  private

  def initial_dimensions(**opts)
    [opts.fetch(:width, DEFAULT_WIDTH),
     opts.fetch(:height, DEFAULT_HEIGHT)]
  end
end
