# frozen_string_literal: true

# The compass brings all the 'world' logic to the gps chip.
# As in, north -> east -> south -> west and how movement while facing a bearing
# impacts the relative (x,y) position.
class Compass
  attr_accessor :bearing

  RULES = {
    north: [0, 1],
    east: [1, 0],
    south: [0, -1],
    west: [-1, 0]
  }.freeze

  def initialize(bearing)
    self.bearing = bearing
  end

  def to_movement
    Struct.new(:x, :y).new(*RULES[bearing])
  end

  def left
    index = (RULES.keys.index(bearing) - 1) % RULES.keys.count
    RULES.keys[index]
  end

  def right
    index = (RULES.keys.index(bearing) + 1) % RULES.keys.count
    RULES.keys[index]
  end
end
