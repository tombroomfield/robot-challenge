# frozen_string_literal: true

require 'set'
require_relative 'broomfield_robot/board'
require_relative 'broomfield_robot/dimensions'
require_relative 'broomfield_robot/gps_chip'
require_relative 'broomfield_robot/compass'
require_relative 'broomfield_robot/robot'
require_relative 'broomfield_robot/terminal_reporter'
require_relative 'broomfield_robot/terminal_receiver'

# Errors
require_relative 'broomfield_robot/exceptions/invalid_dimensions_error'
