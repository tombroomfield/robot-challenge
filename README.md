# Robot challenge
Thomas Broomfield (tomplbroomfield@gmail.com)

## Setup
To run:
1. Navigate to the root of the project.
2. Install bundler if not installed.
3. Run `bundle install`
4. Run `ruby lib/start.rb`
5. To exit, type `exit` or `cmd+c`

To run specs:
1. Complete steps 1-3 from above.
2. Run `rspec spec`


## Discussion
For me, the big 'decision' when building out this project is _where do you store
the location data._ As in, who is responsible for knowing where the robot is?

It doesn't make sense to store it on the `Board`, as the board should be unaware
of what is actually transversing it. Putting that logic here would couple these
too strongly, making other objects travelling on the same board in the future
difficult to implement.

The temptation is to store this data as values on the `Robot`. While this could
work, I believe it this oversteps what a robot should do and limits the ability
to grow with changing business needs.

'Position' is a relative concept, while the robot may sit at (2,3) relative to
the board it is currently on, it could be at a very different position on a
seperate 'board' simultaneously - this is similar to how you can live in the
south side of a city that is located at the northern tip of a state. Further,
storing the data here could quickly lead to the `Robot` class becoming a
`God object`, responsible for far too many things at once.

To solve this, we need something of a 'join' object between a robot and a board.
I've used a `GpsChip`. A robot has a GpsChip for every board
it is located on, the chip then manages the position of the robot relative to
the board it is responsible for. This concept frees up the rest of application
to hold logic in appropriate places.

Other areas worth noting, clearly terminal interaction wouldn't suffice in a
production application, so robots can be initiated with a `receiver` and a
`renderer`, with the defaults being command line versions.

The `receiver` is responsible for parsing commands and the `renderer` is
responsible for outputting the reports. Robots could then easy be adopted to
read commands from a file, the network or even tactile input
(joysticks, keyboards) without needing to modify the robot class at all.
I believe this is an important separation of concerns.
