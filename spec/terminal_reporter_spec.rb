# frozen_string_literal: true

describe TerminalReporter do
  let(:board) { Board.new }
  let(:chip) { GpsChip.new(board) }

  context 'When the chip has not been placed' do
    let(:result) { TerminalReporter.report(chip) }

    it 'should not report' do
      expect(STDOUT).not_to receive(:puts).with('0,0,NORTH')
      expect(result).to eq 'NOTHING TO REPORT'
    end
  end

  context 'When the chip has been placed' do
    let(:result) { TerminalReporter.report(chip) }
    before do
      chip.place(2, 3, :south)
    end

    it 'should report the placement' do
      expect(STDOUT).to receive(:puts).with('2,3,SOUTH')
      expect(result).to eq '2,3,SOUTH'
    end
  end
end
