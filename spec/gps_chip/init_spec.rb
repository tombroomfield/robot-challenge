# frozen_string_literal: true

describe GpsChip do
  let(:board) { Board.new }

  describe '#initialize' do
    context 'When I create a GpsChip with no options' do
      let(:chip) { GpsChip.new(board) }

      before do
        chip.place(0, 0, :north)
      end

      it 'should be dialed in to the supplied board' do
        expect(chip.board).to eq board
      end
    end
  end
end
