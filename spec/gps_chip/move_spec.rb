# frozen_string_literal: true

describe GpsChip do
  let(:board) { Board.new }
  let(:chip) { GpsChip.new(board) }

  describe '#move' do
    context 'When the chip is facing north' do
      context 'With room to move' do
        before do
          chip.place(2, 2, :north)
          chip.move
        end

        it 'should have a new position one place on the y' do
          expect(chip.position.x).to eq 2
          expect(chip.position.y).to eq 3
        end
      end

      context 'With no room to move' do
        before do
          chip.place(2, board.dimensions.height, :north)
          chip.move
        end

        it 'should not move' do
          expect(chip.position.x).to eq 2
          expect(chip.position.y).to eq 5
        end
      end
    end

    context 'When the chip is facing west' do
      context 'With room to move' do
        before do
          chip.place(2, 2, :west)
          chip.move
        end

        it 'should have a new position negative one place on the x' do
          expect(chip.position.x).to eq 1
          expect(chip.position.y).to eq 2
        end
      end

      context 'With no room to move' do
        before do
          chip.place(0, 2, :west)
          chip.move
        end

        it 'should not move' do
          expect(chip.position.x).to eq 0
          expect(chip.position.y).to eq 2
        end
      end
    end

    context 'When the chip is facing south' do
      context 'With room to move' do
        before do
          chip.place(2, 2, :south)
          chip.move
        end

        it 'should have a new position negative one place on the y' do
          expect(chip.position.x).to eq 2
          expect(chip.position.y).to eq 1
        end
      end

      context 'With no room to move' do
        before do
          chip.place(2, 0, :south)
          chip.move
        end

        it 'should not move' do
          expect(chip.position.x).to eq 2
          expect(chip.position.y).to eq 0
        end
      end
    end

    context 'When the chip is facing east' do
      context 'With room to move' do
        before do
          chip.place(2, 2, :east)
          chip.move
        end

        it 'should have a new position one place on the x' do
          expect(chip.position.x).to eq 3
          expect(chip.position.y).to eq 2
        end
      end

      context 'With no room to move' do
        before do
          chip.place(board.dimensions.width, 2, :east)
          chip.move
        end

        it 'should not move' do
          expect(chip.position.x).to eq 5
          expect(chip.position.y).to eq 2
        end
      end
    end
  end
end
