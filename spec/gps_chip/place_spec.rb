# frozen_string_literal: true

describe GpsChip do
  let(:board) { Board.new }
  describe '#place' do
    let(:chip) { GpsChip.new(board) }

    context 'When placed correctly within the board' do
      before do
        chip.place(2, 2, :north)
      end

      it 'should be placed correctly' do
        expect(chip.position.x).to eq 2
        expect(chip.position.y).to eq 2
        expect(chip.bearing).to eq :north
      end
    end

    context 'When placed outside of the x bounds' do
      before do
        chip.place(6, 2, :north)
      end

      it 'should not be placed' do
        expect(chip.position).to be nil
      end
    end

    context 'When placed outside of the y bounds' do
      before do
        chip.place(2, 6, :north)
      end

      it 'should not be placed' do
        expect(chip.position).to be nil
      end
    end

    context 'When the bearing is not valid' do
      before do
        chip.place(2, 2, :worth)
      end

      it 'should not be placed' do
        expect(chip.position).to be nil
      end
    end
  end
end
