# frozen_string_literal: true

describe GpsChip do
  let(:board) { Board.new }
  let(:chip) { GpsChip.new(board) }

  describe '#left' do
    context 'When the chip is facing north' do
      before do
        chip.place(0, 0, :north)
        chip.left
      end

      it 'should now be facing west' do
        expect(chip.bearing).to eq :west
      end
    end

    context 'When the chip is facing south' do
      before do
        chip.place(0, 0, :south)
        chip.left
      end

      it 'should now be facing east' do
        expect(chip.bearing).to eq :east
      end
    end

    context 'When the chip is facing east' do
      before do
        chip.place(0, 0, :east)
        chip.left
      end

      it 'should now be facing north' do
        expect(chip.bearing).to eq :north
      end
    end

    context 'When the chip is facing west' do
      before do
        chip.place(0, 0, :west)
        chip.left
      end

      it 'should now be facing south' do
        expect(chip.bearing).to eq :south
      end
    end
  end

  describe '#right' do
    context 'When the chip is facing east' do
      before do
        chip.place(0, 0, :east)
        chip.right
      end

      it 'should now be facing south' do
        expect(chip.bearing).to eq :south
      end
    end

    context 'When the chip is facing west' do
      before do
        chip.place(0, 0, :west)
        chip.right
      end

      it 'should now be facing north' do
        expect(chip.bearing).to eq :north
      end
    end

    context 'When the chip is facing north' do
      before do
        chip.place(0, 0, :north)
        chip.right
      end

      it 'should now be facing east' do
        expect(chip.bearing).to eq :east
      end
    end

    context 'When the chip is facing south' do
      before do
        chip.place(0, 0, :south)
        chip.right
      end

      it 'should now be facing west' do
        expect(chip.bearing).to eq :west
      end
    end
  end
end
