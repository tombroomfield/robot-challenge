# frozen_string_literal: true

describe Board do
  describe '#dimensions' do
    context 'When I initialise a board with no options' do
      let(:board) { Board.new }

      it 'should default to a width and height of the default values' do
        expect(board.dimensions.width).to eq 5
        expect(board.dimensions.height).to eq 5
      end
    end

    context 'When I explicitly set a width' do
      let(:board) { Board.new(width: 3) }

      it 'should set the width to the supplied width' do
        expect(board.dimensions.width).to eq 3
        expect(board.dimensions.height).to eq 5
      end
    end

    context 'When I explicitly set a height' do
      let(:board) { Board.new(height: 1) }

      it 'should set the height to the supplied height' do
        expect(board.dimensions.width).to eq 5
        expect(board.dimensions.height).to eq 1
      end
    end

    context 'When I explicitly set both' do
      let(:board) { Board.new(height: 1, width: 10) }

      it 'should set the height to the supplied height' do
        expect(board.dimensions.width).to eq 10
        expect(board.dimensions.height).to eq 1
      end
    end

    context 'When supplying 0 values' do
      it 'should throw an error if I set the width to zero or less' do
        expect do
          Board.new(width: 0)
        end.to raise_error(InvalidDimensionsError)
      end

      it 'should throw an error if I set the height to zero' do
        expect do
          Board.new(width: -1)
        end.to raise_error(InvalidDimensionsError)
      end
    end
  end
end
