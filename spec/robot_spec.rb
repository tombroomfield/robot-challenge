# frozen_string_literal: true

describe Robot do
  let(:robot) { Robot.new }

  describe '#register_board' do
    context 'When I add a board to a robot' do
      let(:board) { Board.new }

      before do
        robot.register_board(board)
      end

      it 'should create a chip' do
        expect(robot.gps_chips.count).to eq 1
      end
    end
  end

  describe '#command' do
    context 'With the default receiver' do
    end
  end
end
