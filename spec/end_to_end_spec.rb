# frozen_string_literal: true

describe 'Supplied contraints (from document)' do
  let(:robot) { Robot.new }
  before do
    robot.register_board(Board.new)
  end

  describe 'a)' do
    let(:expected_result) { '0,1,NORTH' }

    it 'should output 0,1,NORTH' do
      expect(STDOUT).to receive(:puts).with(expected_result)

      robot.receiver.single_command('PLACE 0,0,NORTH')
      robot.receiver.single_command('MOVE')
      expect(robot.receiver.single_command('REPORT')).to eq expected_result
    end
  end

  describe 'b)' do
    let(:expected_result) { '0,0,WEST' }

    it 'should output 0,0,WEST' do
      expect(STDOUT).to receive(:puts).with(expected_result)

      robot.receiver.single_command('PLACE 0,0,NORTH')
      robot.receiver.single_command('LEFT')
      expect(robot.receiver.single_command('REPORT')).to eq expected_result
    end
  end

  describe 'c)' do
    let(:expected_result) { '3,3,NORTH' }

    it 'should output 0,0,WEST' do
      expect(STDOUT).to receive(:puts).with(expected_result)

      robot.receiver.single_command('PLACE 1,2,EAST')
      robot.receiver.single_command('MOVE')
      robot.receiver.single_command('MOVE')
      robot.receiver.single_command('LEFT')
      robot.receiver.single_command('MOVE')
      expect(robot.receiver.single_command('REPORT')).to eq expected_result
    end
  end
end
