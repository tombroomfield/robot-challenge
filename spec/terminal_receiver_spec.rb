# frozen_string_literal: true

describe TerminalReceiver do
  let(:robot) { Robot.new(receiver: TerminalReceiver.new) }

  describe 'simple commands' do
    context 'with valid commands' do
      before do
        expect(robot).to receive(:move)
      end

      it 'should with lower case move' do
        robot.receiver.single_command('move')
      end

      it 'should with upper case move' do
        robot.receiver.single_command('MOVE')
      end

      it 'should with mixed case' do
        robot.receiver.single_command('mOvE')
      end

      it 'should with leading spaces' do
        robot.receiver.single_command('  move')
      end

      it 'should with trailing spaces' do
        robot.receiver.single_command('move  ')
      end
    end

    context 'With invalid commands' do
      before do
        expect(robot).not_to receive(:move)
      end

      it 'should not with random text' do
        robot.receiver.single_command('sijdf')
      end
    end
  end

  describe 'place command' do
    context 'with valid commands' do
      it 'should with a lowercase place' do
        expect(robot).to receive(:place).with('1', '2', 'north')
        robot.receiver.single_command('place 1,2,north')
      end

      it 'should with a uppercase place' do
        expect(robot).to receive(:place).with('1', '2', 'north')
        robot.receiver.single_command('PLACE 1,2,NORTH')
      end

      it 'should with spaces between commas' do
        expect(robot).to receive(:place).with('1', '2', 'north')
        robot.receiver.single_command('place 1, 2, north')
      end
    end

    context 'With invalid commands' do
      it 'should not with a fake bearing' do
        expect(robot).not_to receive(:place)
        robot.receiver.single_command('place 1,2,wouth')
      end
    end
  end
end
